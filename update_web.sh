#!/bin/bash
HEAD_COMMIT=`git rev-parse --short HEAD`
# Run the command to check if the repo has uncommited changes.
git diff-index --quiet HEAD --
if [ $? -eq 1 ]
then
  #Error means dirty
  HEAD_COMMIT="${HEAD_COMMIT}_dirty"
fi
echo ${HEAD_COMMIT}

npm run build
# Force update to the latest that has been pushed up
cd ../ademille.bitbucket.io
git fetch
git reset --hard origin/master

# Delete any existing content
rm -rf pegpyramid

# Jump back to the pegpyramid directory
cd -
cp -r build/ ../ademille.bitbucket.io/pegpyramid
cd ../ademille.bitbucket.io
git add pegpyramid/
git commit -am "Updated Peg Pyramid to ${HEAD_COMMIT}"
git push origin master


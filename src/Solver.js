import {Position, PegTypeEnum} from './Board';

/* Helper function to create a move object that keeps track of start and end
 * position
 * @param startPos The starting position of a move
 * @param endPos The ending position of a move
 */
export function Move(startPos, endPos) {
    this.startPos = startPos;
    this.endPos = endPos;
}

/* Information about a board move
 * @param The name of the move, such as 'up left'
 * @param isValidFunc A function to test if the move is valid
 * @param invertFunc A function to invert the pegs for the move
 * @param moveCreationFunc
 * @return a MoveInfo object
 */
function MoveInfo(name, isValidFunc, invertFunc, moveCreationFunc) {
  this.name = name;
  this.isValidFunc = isValidFunc;
  this.invertFunc = invertFunc;
  this.moveCreationFunc = moveCreationFunc;
}

//////////////////////////////////////////////////
// UP Left functions
//////////////////////////////////////////////////
/** Return the position of a peg if moved up left
 * @param board The game board
 * @param pos The starting position of the move
 * @return The new peg location, if a move is possible or undefined otherwise
 */
function upLeftPosition(board, pos) {
  let position = undefined;
  if ((pos.row - 2 >= 0 && pos.col - 2 >= 0)
    && (board[pos.row - 1][pos.col - 1] === PegTypeEnum.FULL_PEG)
    && (board[pos.row - 2][pos.col - 2] === PegTypeEnum.EMPTY_PEG)) {
    position = new Position(pos.row -2, pos.col -2)
  }

  return position;
}

/** Check if a peg can move up left
 * @param board The game board
 * @param the starting position
 * @return true if the move is valid
 */
function canMoveUpLeft(board, pos) {
  return (upLeftPosition(board, pos) !== undefined);
}

/** Invert the pegs on a board in an up left motion
 * @param board The game board
 * @param pos The starting position of the invert move
 */
function invertUpLeft(board, pos) {
  invert(board, new Position(pos.row, pos.col));
  invert(board, new Position(pos.row - 1, pos.col - 1));
  invert(board, new Position(pos.row - 2, pos.col - 2));
}

//////////////////////////////////////////////////
// Down Right functions
//////////////////////////////////////////////////
/** Return the position of a peg if moved down right
 * @param board The game board
 * @param pos The starting position of the move
 * @return The new peg location, if a move is possible or undefined otherwise
 */
function downRightPosition(board, pos) {
  let position = undefined;
  if ((pos.row + 2 < board.length) && (pos.col + 2 < board.length) &&
    (board[pos.row + 1][pos.col + 1] === PegTypeEnum.FULL_PEG) &&
    (board[pos.row + 2][pos.col + 2] === PegTypeEnum.EMPTY_PEG)) {
    position = new Position(pos.row + 2, pos.col + 2);
  }

  return position;
}

/** Check if a peg can move down right
 * @param board The game board
 * @param the starting position
 * @return true if the move is valid
 */
function canMoveDownRight(board, pos) {
  return (downRightPosition(board, pos) !== undefined);
}

/** Invert the pegs on a board in a down right motion
 * @param board The game board
 * @param pos The starting position of the invert move
 */
function invertDownRight(board, pos) {
  invert(board, new Position(pos.row, pos.col));
  invert(board, new Position(pos.row + 1, pos.col + 1));
  invert(board, new Position(pos.row + 2, pos.col + 2));
}


//////////////////////////////////////////////////
// Up Right functions
//////////////////////////////////////////////////
/** Return the position of a peg if moved up right
 * @param board The game board
 * @param pos The starting position of the move
 * @return The new peg location, if a move is possible or undefined otherwise
 */
function upRightPosition(board, pos) {
  let position = undefined;

  if ((pos.row - 2 >= 0 && pos.col < board[pos.row - 2].length) &&
    (board[pos.row - 1][pos.col] === PegTypeEnum.FULL_PEG) &&
    (board[pos.row - 2][pos.col] === PegTypeEnum.EMPTY_PEG)) {
    position = new Position(pos.row -2, pos.col);
  }

  return position;
}

/** Check if a peg can move up right
 * @param board The game board
 * @param the starting position
 * @return true if the move is valid
 */
function canMoveUpRight(board, pos) {
  return (upRightPosition(board, pos) !== undefined);
}

/** Invert the pegs on a board in an up right motion
 * @param board The game board
 * @param pos The starting position of the invert move
 */
function invertUpRight(board, pos) {
  invert(board, new Position(pos.row, pos.col));
  invert(board, new Position(pos.row - 1, pos.col));
  invert(board, new Position(pos.row - 2, pos.col));
}

//////////////////////////////////////////////////
// Down Left functions
//////////////////////////////////////////////////
/** Return the position of a peg if moved down left
 * @param board The game board
 * @param pos The starting position of the move
 * @return The new peg location, if a move is possible or undefined otherwise
 */
function downLeftPosition(board, pos) {
  let position = undefined;

  if ((pos.row + 2 < board.length) &&
    (board[pos.row + 1][pos.col] === PegTypeEnum.FULL_PEG) &&
    (board[pos.row + 2][pos.col] === PegTypeEnum.EMPTY_PEG)){
    position = new Position(pos.row + 2, pos.col);
  }

  return position;
}

/** Check if a peg can move down left
 * @param board The game board
 * @param the starting position
 * @return true if the move is valid
 */
function canMoveDownLeft(board, pos) {
  return (downLeftPosition(board, pos) !== undefined);
}

/** Invert the pegs on a board in an down left motion
 * @param board The game board
 * @param pos The starting position of the invert move
 */
function invertDownLeft(board, pos) {
  invert(board, new Position(pos.row, pos.col));
  invert(board, new Position(pos.row + 1, pos.col));
  invert(board, new Position(pos.row + 2, pos.col));
}


//////////////////////////////////////////////////
// Left functions
//////////////////////////////////////////////////
//Return true if can move left

/** Return the position of a peg if moved left
 * @param board The game board
 * @param pos The starting position of the move
 * @return The new peg location, if a move is possible or undefined otherwise
 */
function leftPosition(board, pos) {
  let position = undefined;

  if ((pos.col - 2 >= 0) &&
    (board[pos.row][pos.col - 1] === PegTypeEnum.FULL_PEG) &&
    (board[pos.row][pos.col - 2] === PegTypeEnum.EMPTY_PEG)
  ) {
    position = new Position(pos.row, pos.col - 2);
  }

  return position;
}

/** Check if a peg can move left
 * @param board The game board
 * @param the starting position
 * @return true if the move is valid
 */
function canMoveLeft(board, pos) {
  // Make sure there is a valid place to move
  return (leftPosition(board, pos) !== undefined);
}


/** Invert the pegs on a board in an left motion
 * @param board The game board
 * @param pos The starting position of the invert move
 */
function invertLeft(board, pos) {
  invert(board, new Position(pos.row, pos.col));
  invert(board, new Position(pos.row, pos.col - 1));
  invert(board, new Position(pos.row, pos.col - 2));
}


//////////////////////////////////////////////////
// Right functions
//////////////////////////////////////////////////

/** Return the position of a peg if moved right
 * @param board The game board
 * @param pos The starting position of the move
 * @return The new peg location, if a move is possible or undefined otherwise
 */
function rightPosition(board, pos) {
  let position = undefined;
  if ((pos.col + 2 < board[pos.row].length) &&
    (board[pos.row][pos.col + 1] === PegTypeEnum.FULL_PEG) &&
    (board[pos.row][pos.col + 2] === PegTypeEnum.EMPTY_PEG)) {
    position = new Position(pos.row, pos.col + 2);
  }

  return position;
}

/** Check if a peg can move right
 * @param board The game board
 * @param the starting position
 * @return true if the move is valid
 */
function canMoveRight(board, pos) {
  return (rightPosition(board, pos) !== undefined);
}

/** Invert the pegs on a board in an right motion
 * @param board The game board
 * @param pos The starting position of the invert move
 */
function invertRight(board, pos) {
  // Move the piece
  invert(board, new Position(pos.row, pos.col));
  invert(board, new Position(pos.row, pos.col + 1));
  invert(board, new Position(pos.row, pos.col + 2));
}

/** Check if there are any moves that can be made on the board
 * @param board The board to check
 * @return true if a move is possible, false otherwise
 */
export function canMove(board) {
  // Loop through every position in the board and see if it can move
  for (let row = 0; row < board.length; row++) {
    for (let col = 0; col < board[row].length; col++) {
      let pos = new Position(row, col);
      if (
        (board[pos.row][pos.col] === PegTypeEnum.FULL_PEG) &&
        (
         canMoveDownLeft(board, pos) ||
         canMoveUpLeft(board, pos) ||
         canMoveDownRight(board, pos) ||
         canMoveUpRight(board, pos) ||
         canMoveLeft(board, pos) ||
         canMoveRight(board, pos)
        )
      ) {
        return true;
      }
    }
  }

  return false;
}

/** Change an empty peg to a full peg and vice versa
 * @param board The board to modify
 * @param row The row position
 * @param col The column position
 */
function invert(board, pos) {
  if (board[pos.row][pos.col] === PegTypeEnum.EMPTY_PEG) {
    board[pos.row][pos.col] = PegTypeEnum.FULL_PEG;
  }
  else {
    board[pos.row][pos.col] = PegTypeEnum.EMPTY_PEG;
  }
}

/** Return the number of pegs left on the board
 * @param board The board to use
 * @return the number of pegs left on the board
 */
export function pegsLeftOnBoard(board) {
  let count = 0;
  for (let row = 0; row < board.length; row++) {
    for (let col = 0; col < board[row].length; col++) {
      if (board[row][col] !== PegTypeEnum.EMPTY_PEG) {
        count++;
      }
    }
  }
  return count;
}

/** Update a board given a starting peg position and an ending peg position
 * @param board  The board that will be updated
 * @param startPos The starting peg position for the move
 * @param endPos The ending peg position for the move
 * @return true if the move succeeded, false if the requested move was invalid
 */
export function makeMove(board, startPos, endPos) {
  let valid = false;
  // Check for destination empty
  if ((board[endPos.row][endPos.col] !== PegTypeEnum.EMPTY_PEG)) {
    valid = false;
  }
  // Check for valid up-left move
  else if ((startPos.row - endPos.row === 2) && (startPos.col - endPos.col === 2) &&
    canMoveUpLeft(board, startPos)) {
    invertUpLeft(board, startPos);
    valid = true;
  }

  // Check for valid down-right move
  else if ((endPos.row - startPos.row === 2) && (endPos.col - startPos.col === 2) &&
    (canMoveDownRight(board, startPos))) {
    invertDownRight(board, startPos);
    valid = true;
  }

  // Check for valid up-right move
  else if ((startPos.row - endPos.row === 2) && (startPos.col === endPos.col) &&
    (canMoveUpRight(board, startPos))) {
    invertUpRight(board, startPos);
    valid = true;
  }

  // Check for valid down-left move
  else if ((endPos.row - startPos.row === 2) && (startPos.col === endPos.col) &&
    (canMoveDownLeft(board, startPos))) {
    invertDownLeft(board, startPos);
    valid = true;
  }

  // Check for valid left
  else if ((startPos.row === endPos.row) && (startPos.col - endPos.col === 2) &&
    (canMoveLeft(board, startPos))) {
    invertLeft(board, startPos);
    valid = true;
  }

  // Check for valid right
  else if ((startPos.row === endPos.row) && (endPos.col - startPos.col === 2) &&
    (canMoveRight(board, startPos))) {
    invertRight(board, startPos);
    valid = true;
  }

  return valid;
}

/** Rescursively solve the puzzle
* @param board The board
* @param moveInfoList A list of possible moves and how to make those moves
* @param firstPos The position of the first peg that was removed
* @param solutionTest A tester to know when success has been achieved
* @return The list of moves for the solution or undefined if no solution is possible
*/
function recursiveSolve(board, moveInfoList, firstPos, solutionTest) {
  // The list of moves to win
  let moveList = undefined;
  // Check to see if we have reached the desired end state
  if (solutionTest(board, firstPos)) {
    // Return an empty array. It will be defined to indicate successful solve.
    return [];
  } else {
    // Iterate through each position trying to solve the puzzle
    for (let row = 0; row < board.length; row++) {
      for (let col = 0; col < board[row].length; col++) {
        if (board[row][col] !== PegTypeEnum.EMPTY_PEG) {
          // Iterate through each possible move
          for (let m = 0; m < moveInfoList.length; m++) {
            let move = moveInfoList[m];
            let pos = new Position(row, col);
            if (move.isValidFunc(board, pos)) {
              move.invertFunc(board, pos);
              moveList = recursiveSolve(board, moveInfoList, firstPos, solutionTest);
              if (moveList !== undefined) {
                // Add the successful move
                let tmpMove = move.moveCreationFunc(pos);
                moveList.push(tmpMove);
                return moveList;
              } else {
                // Undo last step
                move.invertFunc(board, pos);
              }
            }
          } // end iterate through each possible move
        } // end empty peg check
      } // end for each column
    } // end for each row
    // No solution found
    return undefined;
  } // end else
} // end solve


/** A solution tester that returns true if there is a single peg left on the board
 * @param board The board to test
 * @param firstPos The position of the first peg that was removed
 */
export function onePegLeftSolution(board, firstPos) {
  return pegsLeftOnBoard(board) === 1;
}

/** A solution tester that returns true if there is a single peg left on the
 * board and it is left in the position of the first peg that was removed
 * @param board The board to test
 * @param firstPos The position of the first peg that was removed
 */
export function onePegLeftFirstPosition(board, firstPos) {
  if (pegsLeftOnBoard(board) === 1) {
    // Find the last peg
    for (let row = 0; row < board.length; row++) {
      for (let col = 0; col < board[row].length; col++) {
        if (board[row][col] !== PegTypeEnum.EMPTY_PEG) {
          if (firstPos.row === row && firstPos.col === col) {
            return true;
          }
        } // end empty check
      } // end for each column
    } // end for each row
  }  // end one peg left check.

  // Not a solution
  return false;
}

/** A solution tester that returns true if there are eight pegs left with no
 * further moves possible
 * @param board The board to test
 * @param firstPos The position of the first peg that was removed
 */
export function eightPegsLeftSolution(board, firstPos) {
  return pegsLeftOnBoard(board) === 8 && !canMove(board);
}

/** Get the list of valid moves, given a starting point.
 * @param board The board to test
 * @param startPos The position of the peg that will be moved
 * @return A list of possible moves
 */
export function getValidMoveList(board, startPos) {
  // Get a list of valid moves for a given board based on a starting position
  let validMoveList = [];
  let pos = undefined;
  pos = downLeftPosition(board, startPos);
  if (pos !== undefined) {
    validMoveList.push(pos);
  }
  pos = upLeftPosition(board, startPos);
  if (pos !== undefined) {
    validMoveList.push(pos);
  }
  pos = downRightPosition(board, startPos);
  if (pos !== undefined) {
    validMoveList.push(pos);
  }
  pos = upRightPosition(board, startPos);
  if (pos !== undefined) {
    validMoveList.push(pos);
  }
  pos = leftPosition(board, startPos);
  if (pos !== undefined) {
    validMoveList.push(pos);
  }
  pos = rightPosition(board, startPos);
  if (pos !== undefined) {
    validMoveList.push(pos);
  }

  return validMoveList;
}

/** Solve a board, if possible
 * @param board The board to solve
 * @param firstPos the position of the first peg that was removed
 * @param solutionTest A solution tester that will return true when the puzzle
 * is solved
 * @return A list of moves that will result in the solution or undefined if no
 * solution is possible
 */
export function solve(board, firstPos, solutionTest) {
  //Create a list of valid move info
  let moveInfoList = [];
  moveInfoList.push(new MoveInfo("up-left",canMoveUpLeft, invertUpLeft,
                                  function(pos){ return new Move(pos, new Position(pos.row - 2, pos.col - 2));}));
  moveInfoList.push(new MoveInfo("down-right",canMoveDownRight, invertDownRight,
                                  function(pos){ return new Move(pos, new Position(pos.row + 2, pos.col + 2));}));
  moveInfoList.push(new MoveInfo("up-right",canMoveUpRight, invertUpRight,
                                  function(pos){ return new Move(pos, new Position(pos.row - 2, pos.col));}));
  moveInfoList.push(new MoveInfo("down-left",canMoveDownLeft, invertDownLeft,
                                  function(pos){ return new Move(pos, new Position(pos.row + 2, pos.col));}));
  moveInfoList.push(new MoveInfo("left",canMoveLeft, invertLeft,
                                  function(pos){ return new Move(pos, new Position(pos.row, pos.col - 2));}));
  moveInfoList.push(new MoveInfo("left",canMoveRight, invertRight,
                                  function(pos){ return new Move(pos, new Position(pos.row, pos.col + 2));}));

  let moveList = recursiveSolve(board, moveInfoList, firstPos, solutionTest);
  // If the puzzle is solved, the moveList will be returned and reversed due to the recursive calls
  if (moveList !== undefined) {
    moveList.reverse();
  }
  return moveList;
}

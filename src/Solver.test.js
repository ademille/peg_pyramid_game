import * as solver from './Solver';
import {PegTypeEnum, Position} from './Board';

let boardTwoLeft = [
      [1],
     [0,0],
    [0,0,0],
   [0,1,0,0],
  [0,0,0,0,0]
];

let boardOneLeft = [
      [0],
     [0,0],
    [0,0,0],
   [0,1,0,0],
  [0,0,0,0,0]
];

let boardEightLeft = [
      [1],
     [0,1],
    [1,0,1],
   [0,1,0,1],
  [0,0,1,0,1]
];

let boardEightLeftWithMove = [
      [0],
     [1,1],
    [1,0,1],
   [0,1,0,1],
  [0,0,1,0,1]
];


/* Boards with a move available */
let boardDownLeftMove = [
      [1],
     [1,0],
    [0,0,0],
   [0,0,0,0],
  [0,0,0,0,0]
];

let boardUpLeftMove = [
      [0],
     [0,0],
    [0,0,0],
   [0,1,0,0],
  [0,0,1,0,0]
];

let boardDownRightMove = [
      [1],
     [0,1],
    [0,0,0],
   [0,0,0,0],
  [0,0,0,0,0]
];

let boardUpRightMove = [
      [0],
     [0,0],
    [1,0,0],
   [1,0,0,0],
  [0,0,0,0,0]
];

let boardLeftMove = [
      [0],
     [0,0],
    [0,0,0],
   [0,0,1,1],
  [0,0,0,0,0]
];

let boardRightMove = [
      [0],
     [0,0],
    [1,1,0],
   [0,0,0,0],
  [0,0,0,0,0]
];


let boardNoMoves = [
      [1],
     [0,0],
    [1,0,1],
   [0,0,0,0],
  [0,1,0,1,0]
];


it('creates Move object', () => {
  const startPos = new Position(1,2);
  const endPos = new Position(3,4);
  const move = new solver.Move(startPos, endPos);
  expect(move.startPos).toBe(startPos);
  expect(move.endPos).toBe(endPos);
});

it('knows one peg left solution', () => {

  // firstMove doesn't matter for this solver
  const firstMove = new solver.Move(0,0);
  expect(solver.onePegLeftSolution(boardTwoLeft, firstMove)).toBe(false);
  expect(solver.onePegLeftSolution(boardOneLeft, firstMove)).toBe(true);
});

it('can move', () => {
  // firstMove doesn't matter for this solver
  expect(solver.canMove(boardDownLeftMove)).toBe(true);
  expect(solver.canMove(boardUpLeftMove)).toBe(true);
  expect(solver.canMove(boardDownRightMove)).toBe(true);
  expect(solver.canMove(boardUpRightMove)).toBe(true);
  expect(solver.canMove(boardLeftMove)).toBe(true);
  expect(solver.canMove(boardRightMove)).toBe(true);

  expect(solver.canMove(boardNoMoves)).toBe(false);
});

it('can count pegs on board', () => {
  // firstMove doesn't matter for this solver
  expect(solver.pegsLeftOnBoard(boardDownLeftMove)).toBe(2);
  expect(solver.pegsLeftOnBoard(boardNoMoves)).toBe(5);
});

it('make move down left', () => {
  let expectedBoard = [
        [0],
       [0,0],
      [1,0,0],
     [0,0,0,0],
    [0,0,0,0,0]
  ];
  const board = boardDownLeftMove;
  const startPos = new Position(0,0);
  const endPos = new Position(2,0);

  expect(solver.makeMove(board, startPos, endPos)).toBe(true);
  expect(solver.makeMove(board, endPos, startPos)).toBe(false);
  expect(board).toEqual(expectedBoard);
});

it('make move up left', () => {
  let expectedBoard = [
        [0],
       [0,0],
      [1,0,0],
     [0,0,0,0],
    [0,0,0,0,0]
  ];
  const board = boardUpLeftMove;
  const startPos = new Position(4,2);
  const endPos = new Position(2,0);

  expect(solver.makeMove(board, startPos, endPos)).toBe(true);
  expect(solver.makeMove(board, endPos, startPos)).toBe(false);
  expect(board).toEqual(expectedBoard);
});

it('make move down right', () => {
  let expectedBoard = [
        [0],
       [0,0],
      [0,0,1],
     [0,0,0,0],
    [0,0,0,0,0]
  ];
  const board = boardDownRightMove;
  const startPos = new Position(0,0);
  const endPos = new Position(2,2);

  expect(solver.makeMove(board, startPos, endPos)).toBe(true);
  expect(solver.makeMove(board, endPos, startPos)).toBe(false);
  expect(board).toEqual(expectedBoard);
});

it('make move up right', () => {
  let expectedBoard = [
        [0],
       [1,0],
      [0,0,0],
     [0,0,0,0],
    [0,0,0,0,0]
  ];
  const board = boardUpRightMove;
  const startPos = new Position(3,0);
  const endPos = new Position(1,0);

  expect(solver.makeMove(board, startPos, endPos)).toBe(true);
  expect(solver.makeMove(board, endPos, startPos)).toBe(false);
  expect(board).toEqual(expectedBoard);
});

it('make move left', () => {
  let expectedBoard = [
        [0],
       [0,0],
      [0,0,0],
     [0,1,0,0],
    [0,0,0,0,0]
  ];
  const board = boardLeftMove;
  const startPos = new Position(3,3);
  const endPos = new Position(3,1);

  expect(solver.makeMove(board, startPos, endPos)).toBe(true);
  expect(solver.makeMove(board, endPos, startPos)).toBe(false);
  expect(board).toEqual(expectedBoard);
});

it('make move right', () => {
  let expectedBoard = [
        [0],
       [0,0],
      [0,0,1],
     [0,0,0,0],
    [0,0,0,0,0]
  ];
  const board = boardRightMove;
  const startPos = new Position(2,0);
  const endPos = new Position(2,2);

  expect(solver.makeMove(board, startPos, endPos)).toBe(true);
  expect(solver.makeMove(board, endPos, startPos)).toBe(false);
  expect(board).toEqual(expectedBoard);
});

it('solves one peg left', () => {
  expect(solver.onePegLeftSolution(boardOneLeft, new Position(0,0))).toBe(true);
  expect(solver.onePegLeftSolution(boardTwoLeft, new Position(0,0))).toBe(false);
});

it('solves one peg left first position', () => {
  expect(solver.onePegLeftFirstPosition(boardOneLeft, new Position(3,1))).toBe(true);
  expect(solver.onePegLeftFirstPosition(boardOneLeft, new Position(3,2))).toBe(false);
  expect(solver.onePegLeftFirstPosition(boardTwoLeft, new Position(0,0))).toBe(false);
});

it('solves 8 pegs left no moves', () => {
  expect(solver.eightPegsLeftSolution(boardEightLeft, new Position(0,0))).toBe(true);
  expect(solver.eightPegsLeftSolution(boardEightLeftWithMove, new Position(0,0))).toBe(false);
});

it('gets valid move list', () => {
  let board = [
        [1],
       [1,0],
      [0,1,0],
     [0,1,1,0],
    [0,0,1,0,0]
  ];

  // down left
  var expectedMoves = [new Position(2, 0)];
  expect(solver.getValidMoveList(board, new Position(0,0))).toEqual(expectedMoves);

  // up left, up right moves
  expectedMoves = [new Position(2, 0), new Position(2, 2)];
  expect(solver.getValidMoveList(board, new Position(4,2))).toEqual(expectedMoves);

  // down left, down right moves
  expectedMoves = [new Position(4, 1), new Position(4, 3)];
  expect(solver.getValidMoveList(board, new Position(2,1))).toEqual(expectedMoves);

  // left move
  expectedMoves = [new Position(3, 0)];
  expect(solver.getValidMoveList(board, new Position(3,2))).toEqual(expectedMoves);

  // right move
  expectedMoves = [new Position(1, 1), new Position(3,3)];
  expect(solver.getValidMoveList(board, new Position(3,1))).toEqual(expectedMoves);
});


it('solves game', () => {
  // Solving modifies the board, so make several copies.
  let board1 = [
        [1],
       [1,1],
      [1,1,1],
     [1,1,1,1],
    [1,0,1,1,1]
  ];

  let board2 = [
        [1],
       [1,1],
      [1,1,1],
     [1,1,1,1],
    [1,0,1,1,1]
  ];

  let board3 = [
        [1],
       [1,1],
      [1,1,1],
     [1,1,1,1],
    [1,0,1,1,1]
  ];

  let badBoard = [
        [1],
       [0,1],
      [0,0,1],
     [0,0,0,1],
    [0,0,0,0,1]
  ];

  var moveList = solver.solve(board1, new Position(4,1), solver.onePegLeftSolution);
  expect(moveList.length).toBe(13);
  expect(solver.pegsLeftOnBoard(board1)).toBe(1);

  moveList = solver.solve(board2, new Position(4,1), solver.onePegLeftFirstPosition);
  expect(moveList.length).toBe(13);
  expect(solver.pegsLeftOnBoard(board2)).toBe(1);
  expect(board2[4][1]).toBe(PegTypeEnum.FULL_PEG);

  moveList = solver.solve(board3, new Position(4,1), solver.eightPegsLeftSolution);
  expect(moveList.length).toBe(6);
  expect(solver.pegsLeftOnBoard(board3)).toBe(8);
});


import React, { Component } from 'react';
import Board, {PegTypeEnum} from './Board';
import * as solver from './Solver';
import './Game.css';
import './History.css';
import { Button, ButtonToolbar, DropdownButton, MenuItem, Modal, Panel, Pagination} from 'react-bootstrap';

/** An enumeration to represent the state of the game*/
const GameStateEnum = {
  FIRST_MOVE    : 0, /** Waiting for a user to remove the first peg */
  START_JUMP    : 1, /** Waiting for a user to click the peg that will be used to jump */
  END_JUMP      : 2, /** Waiting for a user to click the destination hole for a jump */
  NO_MOVES_LEFT : 3, /** No moves are left on the board */
  NO_SOLUTION   : 4  /** Auto-solve was unable to find a solution */
}


/** A react component that is the Peg Pyramid Game. It pulls in several other
  * components to contruct the game and maintains the state logic and event
  * handlers. */
class Game extends Component {


  showRestartDialog (show){
    this.setState({showRestartDialog:show});
  };

  showHelpDialog (show){
    this.setState({showHelpDialog:show});
  };

  constructor(){
    super();

    this.state = this.getNewGameState();

  }

  /** This is called by the react framework after a component is updated and
    * allows the history board to be scrolled. Without using this method, the
    * state would have to be updated while a component is being rendered, which
    * isn't allowed. */
  componentDidUpdate() {
    // Keep track of the last move element so we can scroll to it.
    const currentBoardRef = this.refs.currentBoardRef;
    if (currentBoardRef !== undefined) {
      //The following works in firefox, but not chrome, for some reason.
      //currentBoardRef.scrollIntoView({block: 'end', behavior: 'smooth'});
      //This works in both, but just isn't smooth.
      currentBoardRef.scrollIntoView();
    }
  }


  /** Initialize all the game state.  This is used by the constructor and also
    * when starting a new game. */
  getNewGameState() {
    let fullBoard = [
      [0],
      [0,0],
      [0,0,0],
      [0,0,0,0],
      [0,0,0,0,0]
    ];

    fullBoard.forEach(function(row){row.fill(PegTypeEnum.FULL_PEG)});

    let state = {
      history: [{
        board: fullBoard,
        move: undefined, // A starting point and stopping point for example (0,0) to (2,0)
      }],
      firstMovePos: undefined, //The location of the first move. Needed for some solutions.
      gameState: GameStateEnum.FIRST_MOVE,
      stepNumber: 0,
      startPegPos: undefined,
      validMoveList: undefined, // A list of valid moves from the current start selection
      showRestartDialog: false,
      showHelpDialog: false,
      historyMove : false // True if the board was selected from history
    };

    return state;
  }

  /** Restart the game */
  restart() {
    this.setState(this.getNewGameState());
  }


  /** The main event handler for the game. Most rendering is triggered by mouse clicks on the board. */
  handleClick(position) {
    // Make a copy of all the history.
    const history = this.state.history.slice(0, this.state.stepNumber + 1)
    const current = history[history.length - 1];
    // Make a deep copy of the board array
    const board = current.board.slice().map(function(arr) {
      return arr.slice();
    });
    let gameState = this.state.gameState;
    let firstMovePos = this.state.firstMovePos;
    let startPegPos = undefined;
    let move = undefined; // Defined if a valid move occurs
    let validMoveList = undefined;

    switch(gameState) {
      // On the first move, just remove a peg
    case GameStateEnum.FIRST_MOVE:
      gameState = GameStateEnum.START_JUMP;
      // Keep track of first move position for scoring
      firstMovePos = position;
      board[position.row][position.col] = PegTypeEnum.EMPTY_PEG;
      // Start is undefined for first move
      move = new solver.Move(undefined, position);
      break;
    case GameStateEnum.START_JUMP:
    case GameStateEnum.NO_SOLUTION: // No solution should still allow moves
      // Check for full source
      if (board[position.row][position.col] === PegTypeEnum.FULL_PEG) {
        gameState = GameStateEnum.END_JUMP;
        startPegPos = position;
        validMoveList = solver.getValidMoveList(board, startPegPos);
      }
      break;
    case GameStateEnum.END_JUMP:
      // Check if a full and update the starting peg
      if (board[position.row][position.col] === PegTypeEnum.FULL_PEG) {
        startPegPos = position;
        validMoveList = solver.getValidMoveList(board, startPegPos);
      }
      // Check is move is valid
      else if (solver.makeMove(board, this.state.startPegPos, position)){
        // Move was valid so clear the start peg state
        startPegPos = undefined;
        // Setup for another start peg
        gameState = GameStateEnum.START_JUMP;
        // Create the move for history
        move = new solver.Move(this.state.startPegPos, position);

        // Check if there are still moves that can be made
        if (!solver.canMove(board)){
          gameState = GameStateEnum.NO_MOVES_LEFT;
        }
      }
      break;
    case GameStateEnum.NO_MOVES_LEFT:
      // Nothing to do here.
      break;
    default:
      break;
    }

    // Only create a new copy of state if it has changed
    if (move !== undefined) {
      this.setState({
        history: history.concat([{
          board: board,
          move: move
        }]),
        firstMovePos: firstMovePos,
        stepNumber: history.length,
      });
    }

    //Always update the state and startPegPos
    this.setState({
      gameState: gameState,
      startPegPos: startPegPos,
      validMoveList: validMoveList,
      historyMove: false //The main board was clicked, so leave history mode
    });
  }

  /** This method will automatically solve a board, if possible
   * @param {Board} board - The board to solve
   * @param {Solution Test} solutionTest - The function that defines success for a solution.
   */
  handleSolve(board, solutionTest) {
    // Make a copy of the board since the solver will modify it
    let solveBoard = JSON.parse(JSON.stringify(board));
    // Make a copy of all the history.
    let history = this.state.history.slice(0, this.state.stepNumber + 1);
    let firstMovePos = this.state.firstMovePos;
    let moveList =  solver.solve(solveBoard, firstMovePos, solutionTest);
    if (moveList !== undefined) {

      // A solution was found. Pretend the user actually clicked all the buttons so that the
      // state and history will be populated correctly.
      let newBoard = board;
      for (let i = 0; i < moveList.length; i++) {
        // Make a deep copy of the board
        newBoard = JSON.parse(JSON.stringify(newBoard));
        solver.makeMove(newBoard, moveList[i].startPos, moveList[i].endPos);
        // Move was valid so clear the start peg state
        // Create the move for history
        let move = new solver.Move(moveList[i].startPos, moveList[i].endPos);

        // Update the history
        history = history.concat([{
          board: newBoard,
          move:move,
        }]);
      }
      // Only create a new copy of state if it has changed
      this.setState({
        history: history,
        stepNumber: history.length - 1,
        gameState: GameStateEnum.NO_MOVES_LEFT,
      });
    } else {
      // No solution possible
      this.setState({
        gameState: GameStateEnum.NO_SOLUTION
      });
    }
  }

  /** Jumps to a specific history steop in the game.
   * @param {number} step - The history step to jump to.
   */
  jumpTo(step) {
    if (step >= 0 && step < this.state.history.length) {
        this.setState({
          historyMove: true // The current board was a history jump
        })
      if (step === 0) {
        this.setState({
          gameState: GameStateEnum.FIRST_MOVE
        })
      }
      else {
        const current = this.state.history[step];
        // Check if there are no valid moves, which means the game is over.
        if (!solver.canMove(current.board)){
          this.setState({
            gameState: GameStateEnum.NO_MOVES_LEFT
          })
        } else {
          // If it's not the first position, then it is a starting jump
          this.setState({
            gameState: GameStateEnum.START_JUMP
          })
        }
      }

      // Change the history selection to the step specified
      this.setState({
        stepNumber: step,
        startPegPos: undefined
      });
    }
  }

  /** Handles a user clicking on a history element and jumps to the appropriate state.
   * @param {number} eventkey - The history selection to jump to (1-based)
   */
  handleHistorySelect(eventKey) {
    this.setState({
      validMoveList: undefined,
    });

    this.jumpTo(eventKey - 1);
  }

  /** Render the buttons that allow moving back and forth in history */
  renderHistoryButtons(stepNumber){
    return (
      <Pagination
        bsClass="pagination with-bottom-margin-only"
        first
        prev
        next
        last
        maxButtons = {1}
        ellipsis = {false}
        items = {this.state.history.length}
        activePage={stepNumber + 1}
        onSelect={(eventKey) => this.handleHistorySelect(eventKey)}
      />
    );
  }

  /** Render each history step as a small board */
  renderHistoryBoards(current, history){

    const moves = history.map((move, i) => {
      return (
        <div ref={current === move ? "currentBoardRef":""} key={i} className={current === move ? "history-board current-board":"history-board"}>
          <Button bsStyle="primary" bsSize="xsmall" style={{"marginBottom":-25, "marginLeft":5}}
            onClick={() => this.jumpTo(i)}>
            {i+1}
          </Button>
          <Board
            board={move.board}
            firstMovePos={this.state.firstMovePos}
            startPos={move.move === undefined ? undefined : move.move.startPos}
            endPos={move.move === undefined ? undefined :move.move.endPos}
            onClick={() => this.jumpTo(i)}
            small={true}
          />
        </div>
      )
    })

    return moves;
  }

  /** Render the help dialog */
  renderHelpDialog(){
    return (
      <Modal bsSize="small" show={this.state.showHelpDialog} onHide={() => this.showHelpDialog(false)}>
        <Modal.Header className="bg-success" closeButton>
          <Modal.Title>Help</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <h3>Goal</h3>
          <p>
          The goal of the game is to remove as many 'pegs' as possible from
            the board, ideally leaving only one peg left, although there are
            some variations for scoring.</p>
          <br/>
          <h3>Rules</h3>
          <p>
            <div className="centered">
              <img src={process.env.PUBLIC_URL + '/board.png'} height="150" width="150" alt="Board"/>
            </div>
          </p>
         <p>
            <ul>
              <li>When the game starts, click on one of the pegs (circles) to remove it.</li>
              <li>Remove pegs from the board by clicking on a green peg and then clicking on an empty peg location.</li>
              <li>The starting peg will be highlighted in blue.</li>
              <li>Valid end positions will be highlighted in purple.</li>
              <li>When no more jumps are possible, the game is over and a score will be shown.</li>
            </ul>
          </p>
          <h3>Scoring</h3>
          <p>
            After the game ends, the score will be calculated as follows:
            <ul>
              <li>Greater than 3 pegs - 0 points.</li>
              <li>3 pegs left on board - 10 points</li>
              <li>2 pegs left on board - 25 points</li>
              <li>1 peg left on board - 50 points</li>
              <li>1 peg left on board in starting hole - 100 points</li>
              <li>8 pegs left on board - 200 points</li>
            </ul>
          </p>
          <h3>Automatic Solver</h3>
          <p>
            After removing at least one peg, the 'Automatic Solver' menu is availble. You can use it to automatically solve the puzzle.  The solver will use the current board as a starting point, so it may not be possible to solve the given puzzle.
          </p>
          <p>
            If the puzzle is solvable, the moves to solve the puzzle will be listed in the history section.
          </p>
          <h3>History Section</h3>
          <p>
          Each move that is made is recorded in the 'History' section. You can use the arrows to move back and forth between moves, or you can click on a move image and jump to that state.</p>
        </Modal.Body>
        <Modal.Footer>
          <Button onClick={() => this.showHelpDialog(false)}>Close</Button>
        </Modal.Footer>
      </Modal>
    )
  }

  /** Render the restart dialog. It will only be shown if the 'Restart Game'
    * button was clicked. */
  renderRestartDialog(){
    return (
      <Modal bsSize="small" show={this.state.showRestartDialog} onHide={() => this.showRestartDialog(false)}>
        <Modal.Header className="bg-primary" closeButton>
          <Modal.Title>Restart Game?</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <p>Restarting will discard all progress.</p>
        </Modal.Body>
        <Modal.Footer>
          <Button onClick={() => this.restart()}>Restart</Button>
          <Button onClick={() => this.showRestartDialog(false)}>Cancel</Button>
        </Modal.Footer>
      </Modal>
    )
  }

  /** Render the game */
  render() {
    // Show the restart dialog


    const current = this.state.history[this.state.stepNumber];

    //Get a copy of the history
    const history = (this.state.history.slice())

    let status;
    let enableSolveButtons = true;
    let score = "-";

    switch (this.state.gameState) {
    case GameStateEnum.FIRST_MOVE:
      status = "Choose a peg to remove";
      enableSolveButtons = false;
      break;
    case GameStateEnum.START_JUMP:
      status = "Choose a peg to move"
      break;
    case GameStateEnum.END_JUMP:
      status = "Choose where to move peg"
      break;
    case GameStateEnum.NO_MOVES_LEFT:
      enableSolveButtons = false;
      let pegsLeft = solver.pegsLeftOnBoard(current.board);
      if (pegsLeft === 1) {
        status = "Game over. " + pegsLeft + " peg left. "
      }
      else {
        status = "Game over. " + pegsLeft + " pegs left. "
      }
      if (pegsLeft === 3){
        score = "Just So-So : 10 points.";
      } else if (pegsLeft === 2) {
        score = "Above Average : 25 points.";
      } else if (pegsLeft === 1) {
        let lastIsFirst = (
          (this.state.firstMovePos.row === current.move.endPos.row) &&
          (this.state.firstMovePos.col === current.move.endPos.col));
        if (lastIsFirst) {
          score = "Very Very Smart : 100 points.";
        }
        else {
          score = "Very Smart : 50 points.";
        }
      } else if (pegsLeft === 8) {
        score = "Genius : 200 points.";
      }
      else {
        score = "No points";
      }

      break;
    case GameStateEnum.NO_SOLUTION:
      status = "No solution possible"
      break;
    default:
      status = "Unknown game state"
      break;

    }
    const title= (<h1>Triangle Peg Game using React.js</h1>);
    return (
      <div>

        {this.renderRestartDialog()}
        {this.renderHelpDialog()}
        <Panel bsStyle="primary" header={title} className="game_panel with-bottom-margin-only">
          <ButtonToolbar className="with-margin">
            <Button  bsStyle="danger"
              onClick={() => this.showRestartDialog(true)}>
              Restart Game
            </Button>
            <DropdownButton id='auto_solve_btn' bsStyle="primary" title="Automatic Solver" disabled={!enableSolveButtons} >
              <MenuItem
                onSelect={() =>this.handleSolve(current.board, solver.onePegLeftSolution)}>
                1 Peg Left (50 points)
              </MenuItem>
              <MenuItem
                onSelect={() => this.handleSolve(current.board, solver.onePegLeftFirstPosition)}>
                1 Peg Left in Starting Hole (100 points)
              </MenuItem>
              <MenuItem
                onSelect={() => this.handleSolve(current.board, solver.eightPegsLeftSolution)}>
                8 Pegs Left (200 points)
              </MenuItem>
            </DropdownButton>
            <Button  bsStyle="success"
              onClick={() => this.showHelpDialog(true)}>
              Help
            </Button>
          </ButtonToolbar>
          <Board
            board={current.board}
            firstMovePos={this.state.firstMovePos}
            startPegPos={this.state.startPegPos}
            validMoveList={this.state.validMoveList}
            onClick={(position) => this.handleClick(position)}
            startPos={(current.move === undefined || !this.state.historyMove) ? undefined : current.move.startPos}
            endPos={(current.move === undefined || !this.state.historyMove) ? undefined :current.move.endPos}
          />
          {/*startPos={current.move === undefined ? undefined : current.move.startPos}*/}
          {/*endPos={current.move === undefined ? undefined :current.move.endPos}*/}
          <div className="status bg-primary with-margin">
            {status}
            <br/>
            {score}
          </div>
        </Panel>
        <Panel bsStyle="primary" header="History" className="button-bar game_panel" >
          {/* TODO: Figure out a better way to get the history buttons into the title */}
          <div className="centered" style={{"marginTop":-51}}>
            {this.renderHistoryButtons(this.state.stepNumber)}
          </div>
          <div className = "div-scroll">
            {this.renderHistoryBoards(current, history)}
          </div>
        </Panel>
      </div>
    );
  }
}

export default Game;

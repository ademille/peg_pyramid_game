import React, { Component } from 'react';
import './Board.css';

/** An enumeration to define whether a peg on the board is empty or full.*/
export const PegTypeEnum = {
  EMPTY_PEG: 0, // No peg to draw
  FULL_PEG: 1,  // A full peg
}

/** A helper function to create an object with a row and column element. */
export function Position(row, col) {
  this.row = row;
  this.col = col;
}


/** Board is a react component that represents the 15 pegs on the board in a
  * triangle pyramid configuration. */
class Board extends Component {

  getCss(position) {
    var style = "peg ";
    switch(this.props.board[position.row][position.col]) {
      case PegTypeEnum.EMPTY_PEG:
        style += "emptypeg ";
        break;
      case PegTypeEnum.FULL_PEG:
        style += "fullpeg ";
        break;
      default:
        break;
    }

    // Check for special coloring
    if (this.props.startPegPos !== undefined) {
      if (position.row === this.props.startPegPos.row && position.col === this.props.startPegPos.col) {
        style += "startpeg ";
      }
    }

    // Check for first move location to highlight in red
    if (this.props.firstMovePos !== undefined) {
      if (position.row === this.props.firstMovePos.row && position.col === this.props.firstMovePos.col) {
        style += "firstpeg ";
      }
    }

    if (this.props.startPos !== undefined) {
      if (position.row === this.props.startPos.row && position.col === this.props.startPos.col) {
        style += "movestart ";
      }
    }

    if (this.props.endPos !== undefined) {
      if (position.row === this.props.endPos.row && position.col === this.props.endPos.col) {
        style += "moveend ";
      }
    }

    // Check if the pegs should be small (for history display)
    if (this.props.small !== undefined) {
      if (this.props.small) {
        style += "smallpeg";
      }
    }

    // Check if the pegs should be displayed as a valid move
    const validMoveList = this.props.validMoveList;
    if (validMoveList !== undefined) {
      // See if the position is in the list
      for (let i=0; i<validMoveList.length; i++) {
        if (
          (validMoveList[i].row === position.row) &&
          (validMoveList[i].col === position.col))
          style += "validmove";
      }
    }

    return style;
  }

  /** Render a button for a single peg.  Color it according to the state.*/
  renderPeg(position) {
    return (
      <button
      key={position.row*(position.row+1)/2 + (position.col)} //binomial coeeficient
      className={this.getCss(position)}
      onClick={() => this.props.onClick(position)}>
      </button>
    );
  }

  /** Render the board */
  render(){
    var board = [];
    var row = [];
    // Create the Triangle Board
    for (let r = 0; r < this.props.board.length; r++){
      row = []
      for (let c = 0; c < this.props.board[r].length; c++)
      {
          row.push(this.renderPeg(new Position(r, c)));
      }

      board.push(
        <div key={row} className="board-row">
          {row}
        </div>
      );
    }

    var comp = (
      <div className="game-board">
        {board}
      </div>
    );

    return comp;
  }
}

export default Board;

# Peg Pyramid Game using React.js

My kids have a little game called 'the Original IQ Tester'. As I attempted to solve it with varying degrees of success, I did what any self-respecting software engineer would do. I started thinking about creating an algorithm to solve the puzzle. So, I whipped up a program in python that recursively solved the puzzle and would print out the steps.

```
See peg_pyramid_game.py in the root of the repository.
```

![IQ Tester](images/iq_tester.jpg)

I also wanted to learn how to use the React.js framework, bootstrap and some javascript since I hadn't done any web development in a long time. So, this seemed like a great little project that was significant enough to learn something. 

I learned the basics of React.js by following the tutorial.

[React.js tic-tac-toe tutorial](https://reactjs.org/tutorial/tutorial.html)

Try my tic-tac-toe game here:
[Tic-Tac-Toe game based on tutorial](https://ademille.bitbucket.io/tictactoe/)

There's certainly a lot more I can do to polish this, but I'm pretty happy with how things are working. While I'm an experienced software developer, I'm a novice in this realm, so I probably made some rooky mistakes, but this project has been a great way to run into problems and learn how to solve them.

## Screen Shots
Here's an example of playing through a game (rather poorly). Each move is recorded in the history. You can move back an forth through the moves, providing the ability to undo moves.

![Full Game](images/full_game.gif)

Since my attempt to solve the puzzle myself was a failure, I'll use the 'Automatic Solver' this time. The game can handle multiple solvers. I just added in a few.

![Full Game](images/auto_solve.gif)

### Try it live here: [Peg Pyramid Game](https://ademille.bitbucket.io/pegpyramid/)


## Getting Started
This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).
To build and run the code:
* Clone the repository
* Use npm to update and install the required packages
* Use npm to run the application
```
$> git clone https://bitbucket.org/ademille/peg_pyramid_game.git
$> npm update
$> npm run
```

This will popup a web browser with the application running.

I've done all development on MacOS and Linux.

### Prerequisites

You'll need node.js and npm.

## Running the tests

Right now I just have a basic test to make sure the Game component can be instantiated without errors.
I also have extensive tests for the Solver.js class, which is where most of the game logic lives. 

The basic test can be run using:
```
npm test
```

## Deployment

This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).
It provides helper scripts for deployment and doing all the manipulation to create something that can be deployed.
```
$> npm run build
```

Add additional notes about how to deploy this on a live system

## Built With

* [React.js](https://reactjs.org/) - The web framework used
* [React Bootstrap](https://react-bootstrap.github.io/) - Front-end Component for React.js

## Contributing

This was just a project for me to learn React.js and have some fun. Hopefully, it can be helpful to someone else as an example as well. If you'd like to contribute, let me know. 

## Versioning

I haven't worried about versioning on this, since it's not intended to be consumed by anyone and was just for fun.

## Authors

* **Aaron DeMille** - [Bitbucket](https://bitbucket.org/ademille), [Linked-In](https://www.linkedin.com/in/aaron-demille-8252605/)

## License

This project is licensed under the MIT License

## Acknowledgments

* Good tutorials and great open source software
* My sons and nephew, who played the game and helped me learn which parts were confusing for kids

